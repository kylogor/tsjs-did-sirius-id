import { sha3_256 } from 'js-sha3';

import baseX from 'base-x';

const BASE58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz';
const base58 = baseX(BASE58);

export class Security {
  public static sha256Hash(data: Buffer) {
    const sha256 = require('js-sha256').sha256;
    return sha256(data);
  }

  public static sha256StringHash(data: string): string {
    const sha256 = require('js-sha256').sha256;
    return sha256(data);
  }

  public static sha3_256Hash(data: Buffer) {
    return sha3_256(data);
  }

  public static base58Encode(data: Buffer) {
    return base58.encode(data);
  }

  public static base58Decode(data: string) {
    return base58.decode(data);
  }
}
