export const DID_METHOD = 'sirius';

/** ERROR MESSAGES */

export const REGISTRY_PROVIDER_UNAVAILABLE =
  'Registry provider is not available';

export const STORAGE_PROVIDER_UNAVAILABLE = 'Storage provider is not available';

export const INVALID_DID_URL = 'Invalid DID URL';

export const UNABLE_TO_CREATE_SIRIUS_ID = 'Unable to create Sirius Id';

export const VERIFICATION_KEY = 'Ed25519VerificationKey2018';

export const SIRIUS_ID_PUBLIC_PROFILE_ENTITY = 'SiriusIdPublicProfile';

export const SIRIUS_ID_PUBLIC_PROFILE_ENTITY_DESC =
  'Verifiable Credential describing Sirius Id Public Profile';

export const SIRIUS_ID_NOT_FOUND = 'Unable to find Sirius Id record';

export const SIRIUS_ID_DOCUMENT_NOT_FOUND =
  'Unable to retrieve Sirius Id document';

export const SIRIUS_ID_DOCUMENT_EXISTS = 'Document already exists';

export const GENERATE_KEYPAIR_ERROR = 'Unable to generate key pair';
