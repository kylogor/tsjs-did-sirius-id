import { SiriusAccount } from '../sirius-account';
import { DidDocument } from '../dids/document/document';

export type NetworkIdentifer = string;
export type AccountAddress = string;
export type Did = string;
export type Payload = Buffer[];
export type Mnid = string;
export type MnidModel = {
  network: NetworkIdentifer;
  address: AccountAddress;
};

export type Identity = {
  account: SiriusAccount;
  did: string;
  didDocument: DidDocument;
  qrCode?: string;
  referenceTx?: string;
};

export interface IKeyPair {
  privateKey: string;
  publicKey: string;
}

export interface IJsonLdObject {
  '@context'?: JsonLdContext;
  [key: string]: JsonLdPrimitive | JsonLdPrimitive[];
}

export type ContextEntry = string | { [key: string]: ContextEntry };
export type JsonLdPrimitive =
  | string
  | number
  | boolean
  | IJsonLdObject
  | IJsonLdObject[];

export type JsonLdContext = ContextEntry | ContextEntry[];

export type SiriusPublicProfile = {
  hash: string;
  url: string;
};

export type ResourceHash = {
  hash: string;
  url: string;
};
