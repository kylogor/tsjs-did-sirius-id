import { classToPlain, plainToClass, Exclude, Expose } from 'class-transformer';
import { IAuthenticationSection } from './document-common';
import { PublicKeySection } from './public-key';
import 'reflect-metadata';
@Exclude()
export class AuthenticationSection {
  /**
   * Get the id of the public key
   */

  @Expose()
  get publicKey(): string {
    return this._publicKey;
  }

  /**
   * Set the id of the public key
   */

  set publicKey(key: string) {
    this._publicKey = key;
  }

  /**
   * Get the authentication type
   */
  @Expose()
  public get type(): string {
    return this._type;
  }

  /**
   * Set the authentication type
   */
  public set type(type: string) {
    this._type = type;
  }

  /**
   * Creates authentication from public key section
   * @param publicKeySection the public key section
   */
  public static create(
    publicKeySection: PublicKeySection,
  ): AuthenticationSection {
    const authentication = new AuthenticationSection();
    authentication.publicKey = publicKeySection.id;
    authentication.type = publicKeySection.type;

    return authentication;
  }

  private _publicKey: string;
  private _type: string;

  /**
   * Serializes authentication section to JSON
   */
  public toJSON(): IAuthenticationSection {
    return classToPlain(this) as IAuthenticationSection;
  }

  /**
   * Creates an authentication section from json
   * @param json the authentication section in json format
   */
  public fromJSON(json: IAuthenticationSection): AuthenticationSection {
    return plainToClass(AuthenticationSection, json);
  }
}
