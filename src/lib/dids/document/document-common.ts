import { IJsonLdObject } from '../../common/types';

export interface IAuthenticationSection extends IJsonLdObject {
  publicKey: string;
  type: string;
}

export interface IPublicKeySection extends IJsonLdObject {
  id: string;
  type: string;
  publicKeyHex: string;
}

export interface IServiceSection extends IJsonLdObject {
  id: string;
  type: string;
  serviceEndpoint: string;
  description: string;
}

export interface IDidDocument extends ISignedJsonLdObject {
  id: string;
  authentication: IAuthenticationSection[];
  publicKey: IPublicKeySection[];
  service: IServiceSection[];
  created: string;
  updated: string;
}

export interface ISerializable {
  toJSON: () => {};
}

export interface ILinkedDataSignature extends IDigestable, ISerializable {
  creator: string;
  type: string;
  nonce: string;
  created: Date;
}
export interface IDigestable {
  signature: string;
  digest: () => Promise<Buffer>;
  signer: {
    did: string;
    keyId: string;
  };
}

export interface ILinkedDataSignatureAttrs extends IJsonLdObject {
  type: string;
  created: string;
  creator: string;
  nonce: string;
  signatureValue: string;
  id?: string;
}

export interface ISignedJsonLdObject extends IJsonLdObject {
  proof: ILinkedDataSignatureAttrs;
}

export interface ISigner {
  did: string;
  keyId: string;
}
