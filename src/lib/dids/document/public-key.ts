import { Expose, classToPlain, plainToClass, Exclude } from 'class-transformer';
import { IPublicKeySection } from './document-common';
import { VERIFICATION_KEY } from '../../common/constants';
import { Did } from '../../common/types';
import 'reflect-metadata';
@Exclude()
export class PublicKeySection {
  /**
   *
   * @param publicKey The public key in hex format
   * @param id the public key identifier e.g. #authentication-key-1
   * @param did the owner did
   */
  public static create(
    publicKey: string,
    id: Did,
    did: string,
  ): PublicKeySection {
    const publicKeySection = new PublicKeySection();
    publicKeySection.id = id;
    publicKeySection.type = VERIFICATION_KEY;
    publicKeySection.owner = did;
    publicKeySection.publicKeyHex = publicKey;
    return publicKeySection;
  }

  private _id: string;
  private _type: string;
  private _owner: string;
  private _publicKeyHex: string;

  /**
   * Get the did of the public key owner
   */

  @Expose()
  get owner(): string {
    return this._owner;
  }

  /**
   * Set the did of the public key owner
   */

  set owner(owner: string) {
    this._owner = owner;
  }

  /**
   * Get the public key identifier
   */

  @Expose()
  get id(): string {
    return this._id;
  }

  /**
   * Set the public key identifier
   */

  set id(id: string) {
    this._id = id;
  }

  /**
   * Get the public key type
   */

  @Expose()
  get type(): string {
    return this._type;
  }

  /**
   * Set the public key type
   */

  set type(type: string) {
    this._type = type;
  }

  /**
   * Get the public key encoded as hex
   */

  @Expose()
  get publicKeyHex(): string {
    return this._publicKeyHex;
  }

  /**
   * Set the public key
   */

  set publicKeyHex(keyHex: string) {
    this._publicKeyHex = keyHex;
  }

  public toJSON(): IPublicKeySection {
    return classToPlain(this) as IPublicKeySection;
  }

  public fromJSON(json: IPublicKeySection): PublicKeySection {
    return plainToClass(PublicKeySection, json);
  }
}
