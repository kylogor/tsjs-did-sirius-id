import { MnidModel, Did, toMNID, fromMNID } from '../..';
import { INVALID_DID_URL } from '../common/constants';

const DID_METHOD = 'sirius';
const DID_URL_REGEX = /^did:(?<method>[a-z0-9]+):(?<idstring>[A-Za-z0-9\.\-_]+)(?:#(?<fragment>.*))?$/;

/**
 * Creates Did from the method specific identifier
 * @param identifier The unique identifier of the Sirius Chain
 */
export const createDid = ({ network, address }: MnidModel): Did => {
  const idstring = toMNID({ network, address });

  const did = `did:${DID_METHOD}:${idstring}`;

  return did;
};

/**
 * Resolves the network and address information from SiriusID
 * @param siriusId The SiriusID follow the DID scheme
 * {@link https://w3c-ccg.github.io/did-spec/#the-generic-did-scheme DID Document}
 */
export const resolveDid = (siriusId: Did): MnidModel => {
  const decoded = parseDidUrl(siriusId);

  if (decoded.method !== DID_METHOD) {
    throw new TypeError(`Unsupport method '${decoded.method}'`);
  }

  const { idstring } = decoded;

  const mnidModel = fromMNID(idstring);

  return mnidModel;
};

/**
 * Determines the given Url is the did Url
 * @param url The did Url
 */
export const isDidUrl = (url: string): boolean => {
  return DID_URL_REGEX.test(url);
};

/**
 * Parses the Did url and retrieves the information such as
 * method, id string and fragment
 * @param url The Did Url
 */
export const parseDidUrl = (url: string) => {
  const matches = DID_URL_REGEX.exec(url);

  if (matches === null) {
    throw new TypeError(INVALID_DID_URL);
  }

  const [, method, idstring, fragment] = matches;

  return {
    did: `did:${method}:${idstring}`,
    method,
    idstring,
    fragment: fragment || null,
  };
};
