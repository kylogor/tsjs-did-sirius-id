import { NetworkIdentifer, IKeyPair } from '../common/types';
import { toNetworkType } from '../utils/converter';
import { IKeyVaultProvider } from '../common/providers';
import { Account } from 'tsjs-xpx-chain-sdk';

/**
 * @class
 * Sirius Id specific key vault provider
 */
export class KeyVaultProvider implements IKeyVaultProvider {
  /**
   * Generates key pair based on the network identifier
   * @param network the network identifier
   */
  public generateKeyPair(network: NetworkIdentifer): IKeyPair {
    const networkType = toNetworkType(network);
    const account = Account.generateNewAccount(networkType);

    return { privateKey: account.privateKey, publicKey: account.publicKey };
  }

  /**
   * Generates key pair based on the network identifier and master private key
   * @param network the network identifier
   * @param privateKey the private master key
   */
  public createFromMasterKey(
    network: NetworkIdentifer,
    privateKey: string,
  ): IKeyPair {
    const networkType = toNetworkType(network);
    const account = Account.createFromPrivateKey(privateKey, networkType);
    return { privateKey, publicKey: account.publicKey };
  }
}
